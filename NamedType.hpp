#pragma once

// see https://www.fluentcpp.com/2016/12/08/strong-types-for-strong-interfaces/
template<typename T, typename Parameter>
class NamedType
{
public:
    explicit NamedType(T const &value) : value_(value)
    {}

    explicit NamedType(T &&value) : value_(std::move(value))
    {}

    T &value()
    { return value_; }

    T const &value() const
    { return value_; }

private:
    T value_;
};