#pragma once

#include "Cell.hpp"
#include "NamedType.hpp"
#include <vector>
#include <functional>

using CoordinateX = NamedType<size_t, struct InitCoordinateXParameter>;
using CoordinateY = NamedType<size_t, struct InitCoordinateYParameter>;
using NeighbourCount = NamedType<size_t, struct NeigbourCountParameter>;

class ConwaysGame
{
public:
    ConwaysGame() = default;

    void print() const;

    void initCell(const CoordinateX &coordinateX, const CoordinateY &coordinateY);

    void singleRound();

private:
    struct CellBox
    {
        const Cell aliveLastRound{CellType::ALIVE_LAST_ROUND};
        const Cell aliveThisRound{CellType::ALIVE_THIS_ROUND};
        const Cell deadLastRound{CellType::DEAD_LAST_ROUND};
        const Cell deadThisRound{CellType::DEAD_THIS_ROUND};
    };

    class PlaygroundWrapper : public std::vector<std::vector<std::reference_wrapper<const Cell>>>
    {
    public:
        explicit
        PlaygroundWrapper(const Cell &cell)
        {
            for (size_t rowCount = 0; rowCount < PLAYGROUND_SIZE; rowCount++)
            {
                std::vector<std::reference_wrapper<const Cell>> row;
                for (size_t columnCount = 0; columnCount < PLAYGROUND_SIZE; columnCount++)
                {
                    row.push_back(std::ref(cell));
                }
                push_back(row);
            }
        }

        decltype(auto) atCoordinates(CoordinateX coordinateX, CoordinateY coordinateY)
        {
            return at(coordinateY.value()).at(coordinateX.value());
        }
    };

    NeighbourCount neighbourCountFor(const CoordinateX &coordinateX, const CoordinateY &coordinateY);

    static void
    makeDecisionBasedOnNeighbours(const NeighbourCount &neighbourCount,
                                  std::reference_wrapper<const Cell> &currentCell);

    static constexpr size_t PLAYGROUND_SIZE = 10;

    static const CellBox &cellBox();

    PlaygroundWrapper playground{cellBox().deadLastRound};

    static void examineNeighbour(const std::reference_wrapper<const Cell> &cell, size_t &currentNeighboursCount);

    static void endOfRound(std::reference_wrapper<const Cell> &currentCell);
};

