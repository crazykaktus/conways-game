#include <iostream>
#include <functional>
#include "ConwaysGame.hpp"

void ConwaysGame::print() const
{
    std::for_each(playground.begin(), playground.end(), [&](const auto &row) {
        std::for_each(row.begin(), row.end(), [&](const auto &column) {
            std::cout << column.get().to_string();
        });
        std::cout << "\n";
    });
    std::cout << std::endl;
}

void ConwaysGame::initCell(const CoordinateX &coordinateX, const CoordinateY &coordinateY)
{
    playground.at(coordinateY.value()).at(coordinateX.value()) = cellBox().aliveLastRound;
}

void ConwaysGame::singleRound()
{
    for (size_t indexY = 0; indexY < playground.size(); indexY++)
    {
        for (size_t indexX = 0; indexX < playground.front().size(); indexX++)
        {
            const NeighbourCount neighbourCount = neighbourCountFor(CoordinateX(indexX), CoordinateY(indexY));
            makeDecisionBasedOnNeighbours(neighbourCount,
                                          playground.atCoordinates(CoordinateX(indexX), CoordinateY(indexY)));
        }
    }

    for (size_t indexY = 0; indexY < playground.size(); indexY++)
    {
        for (size_t indexX = 0; indexX < playground.front().size(); indexX++)
        {
            endOfRound(playground.atCoordinates(CoordinateX(indexX), CoordinateY(indexY)));
        }
    }
}

NeighbourCount ConwaysGame::neighbourCountFor(const CoordinateX &coordinateX, const CoordinateY &coordinateY)
{
    size_t count = 0;

    if (coordinateX.value() > 0)
    {
        if (coordinateY.value() > 0)
        {
            examineNeighbour(playground.atCoordinates(
                                     CoordinateX(coordinateX.value() - 1),
                                     CoordinateY(coordinateY.value() - 1)),
                             count);
        }

        examineNeighbour(playground.atCoordinates(
                                 CoordinateX(coordinateX.value() - 1),
                                 coordinateY),
                         count);
        if (coordinateY.value() < PLAYGROUND_SIZE - 1)
        {
            examineNeighbour(playground.atCoordinates(
                                     CoordinateX(coordinateX.value() - 1),
                                     CoordinateY(coordinateY.value() + 1)),
                             count);
        }
    }

    if (coordinateY.value() > 0)
    {
        examineNeighbour(playground.atCoordinates(
                                 coordinateX,
                                 CoordinateY(coordinateY.value() - 1)),
                         count);
    }

    if (coordinateY.value() < PLAYGROUND_SIZE - 1)
    {
        examineNeighbour(playground.atCoordinates(
                                 coordinateX,
                                 CoordinateY(coordinateY.value() + 1)),
                         count);
    }

    if (coordinateX.value() < PLAYGROUND_SIZE - 1)
    {
        if (coordinateY.value() > 0)
        {
            examineNeighbour(playground.atCoordinates(
                                     CoordinateX(coordinateX.value() + 1),
                                     CoordinateY(coordinateY.value() - 1)),
                             count);
        }

        examineNeighbour(playground.atCoordinates(
                                 CoordinateX(coordinateX.value() + 1),
                                 coordinateY),
                         count);

        if (coordinateY.value() < PLAYGROUND_SIZE - 1)
        {
            examineNeighbour(playground.atCoordinates(
                                     CoordinateX(coordinateX.value() + 1),
                                     CoordinateY(coordinateY.value() + 1)),
                             count);
        }
    }

    return NeighbourCount(count);
}

void ConwaysGame::makeDecisionBasedOnNeighbours(const NeighbourCount &neighbourCount,
                                                std::reference_wrapper<const Cell> &currentCell)
{
    switch (neighbourCount.value())
    {
        case 0:
        case 1:
            if (currentCell.get().cellType == CellType::ALIVE_LAST_ROUND)
            {
                currentCell = cellBox().deadThisRound;
            }
            break;
        case 2:
            break;
        case 3:
            if (currentCell.get().cellType == CellType::DEAD_LAST_ROUND)
            {
                currentCell = cellBox().aliveThisRound;
            }
            break;
        case 4:
        case 5:
        case 6:
        case 7:
        case 8:
            if (currentCell.get().cellType != CellType::DEAD_LAST_ROUND)
            {
                currentCell = cellBox().deadThisRound;
            }
            break;
        default:
            throw std::exception();

    }
}

void ConwaysGame::examineNeighbour(const std::reference_wrapper<const Cell> &cell, size_t &currentNeighboursCount)
{
    switch (cell.get().cellType)
    {
        case CellType::ALIVE_LAST_ROUND:
            currentNeighboursCount++;
            break;
        case CellType::ALIVE_THIS_ROUND:
        case CellType::DEAD_LAST_ROUND:
            break;
        case CellType::DEAD_THIS_ROUND:
            currentNeighboursCount++;
            break;
        default:
            throw std::exception(); //TODO
    }
}

void ConwaysGame::endOfRound(std::reference_wrapper<const Cell> &currentCell)
{
    if (currentCell.get().cellType == cellBox().deadThisRound.cellType)
    {
        currentCell = cellBox().deadLastRound;
    }
    else if (currentCell.get().cellType == cellBox().aliveThisRound.cellType)
    {
        currentCell = cellBox().aliveLastRound;
    }
}

const ConwaysGame::CellBox &ConwaysGame::cellBox()
{
    static const CellBox cellBox;
    return cellBox;
}
