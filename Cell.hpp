#pragma once

#include <utility>

enum class CellType
{
    ALIVE_LAST_ROUND,
    ALIVE_THIS_ROUND,
    DEAD_LAST_ROUND,
    DEAD_THIS_ROUND,
};

class Cell
{
public:
    const CellType cellType;

    explicit
    Cell(CellType inputCellType, std::string aliveSymbol = "[*]", std::string deadSymbol = "[ ]") : cellType(
            inputCellType),
                                                                                                    symbol((inputCellType ==
                                                                                                            CellType::ALIVE_LAST_ROUND) ||
                                                                                                           (inputCellType ==
                                                                                                            CellType::ALIVE_THIS_ROUND)
                                                                                                           ? std::move(
                                                                                                                    aliveSymbol)
                                                                                                           : std::move(
                                                                                                                    deadSymbol))
    {}

    [[nodiscard]]
    std::string to_string() const
    {
        return symbol;
    }

private:
    std::string symbol;
};
