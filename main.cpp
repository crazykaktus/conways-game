#include <iostream>
#include "ConwaysGame.hpp"

int main()
{
    ConwaysGame conwaysGame;
    conwaysGame.print();
    //blinker
    conwaysGame.initCell(CoordinateX(2), CoordinateY(3));
    conwaysGame.initCell(CoordinateX(1), CoordinateY(3));
    conwaysGame.initCell(CoordinateX(3), CoordinateY(3));

    //beacon
    conwaysGame.initCell(CoordinateX(7), CoordinateY(3));
    conwaysGame.initCell(CoordinateX(7), CoordinateY(4));
    conwaysGame.initCell(CoordinateX(6), CoordinateY(3));
    conwaysGame.initCell(CoordinateX(6), CoordinateY(4));

    conwaysGame.initCell(CoordinateX(8), CoordinateY(5));
    conwaysGame.initCell(CoordinateX(8), CoordinateY(6));
    conwaysGame.initCell(CoordinateX(9), CoordinateY(5));
    conwaysGame.initCell(CoordinateX(9), CoordinateY(6));

    //toad
    conwaysGame.initCell(CoordinateX(2), CoordinateY(8));
    conwaysGame.initCell(CoordinateX(3), CoordinateY(8));
    conwaysGame.initCell(CoordinateX(4), CoordinateY(8));
    conwaysGame.initCell(CoordinateX(3), CoordinateY(7));
    conwaysGame.initCell(CoordinateX(4), CoordinateY(7));
    conwaysGame.initCell(CoordinateX(5), CoordinateY(7));

    conwaysGame.print();

    conwaysGame.singleRound();
    conwaysGame.print();
    conwaysGame.singleRound();
    conwaysGame.print();
    conwaysGame.singleRound();
    conwaysGame.print();
    conwaysGame.singleRound();
    conwaysGame.print();
    return 0;
}
